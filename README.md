# LinAlg Appendix
This repo is for the appendix that will be available in the LinAlg exam.

# How To Contribute
First clone the repo, preferably with ssh (if you don't know what that is, you can just use HTTP), into a directory. For this open a terminal 
and navigate to the folder where you want the project to be. Then clone the repo with the following command

``` sh
# (ssh)
git clone git@gitlab.ethz.ch:glaager/lin-alg-py-appendix.git

# (HTTP)
git clone https://gitlab.ethz.ch/glaager/lin-alg-py-appendix.git
```


When you open the folder in for example VSCode you will be able to use the git plugin as usual.
